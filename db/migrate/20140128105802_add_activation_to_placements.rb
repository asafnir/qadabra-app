class AddActivationToPlacements < ActiveRecord::Migration
  def change
  	change_table(:placements) do |t|
      t.boolean :active, null: false, default: true
    end
  end
end
