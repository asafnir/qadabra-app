class CreatePlacements < ActiveRecord::Migration
  def change
    create_table :placements do |t|
      t.string :name
      t.string :size
      t.string :url
      t.integer :owner_id

      t.timestamps
    end
    add_index :placements, :owner_id
  end
end
