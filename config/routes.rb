QadabaraApp::Application.routes.draw do

  authenticated :user do
    root :to => 'dashboard#index'
  end

  root :to => "home#index"
  
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

  devise_for :users

  namespace :api, defaults: {format: :json} do
    devise_scope :user do
      resource :session, only: [:create, :destroy]
      get '/current_user' => 'sessions#show_current_user', as: 'show_current_user'
    end
    resources :placements, only: [:index, :create, :update, :destroy, :show]
  end

  resources :users, only: [:index, :show, :destroy, :update]
  
  get "dashboard" => "dashboard#index"
  get "placements/:id" => 'api/placements#show'
end
