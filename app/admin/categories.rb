ActiveAdmin.register Category do
  menu :parent => 'Configurations'  

  index do
    column :id
    column :name
    default_actions
  end

  form do |f|
    f.inputs "Category" do
      f.input :name
    end

    f.actions
  end

end