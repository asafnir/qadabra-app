angular.module('qadabraServices', ['ngResource'])
  .factory('Dashboard', ['$resource', ($resource) ->
    $resource('/api/placements', {ajax: 1}, {
       query: {method: 'GET', isArray: false}
    })
  ])
  .factory('Placement', ['$resource', ($resource) ->
    $resource('/api/placements/:placement_id', {}, {
      query: {method: 'GET', isArray: true},
      get: {method: 'GET', isArray: false},
      create: {method: 'POST', isArray: false}
      update: {method: 'PUT',params: {placement_id: "@id"}, isArray: false}
    })
  ])
  .factory('Dialog', () -> new DialogServices())

window.$$qadabraAppModule = angular.module('qadabraApp', [
  'ngResource',
  'localytics.directives', 
  'qadabraServices', 
  'ui.router', 
  'ui.bootstrap',
  'ngTable',
  'googlechart'
])


# Qadabra app angular config
window.$$qadabraAppModule.config(['$httpProvider', ($httpProvider) ->
    $httpProvider.defaults.headers.common['X-CSRF-Token']= $("meta[name='csrf-token']").attr("content")
    $httpProvider.defaults.headers.common['Content-Type']= 'application/json'
    $httpProvider.defaults.headers.common['Pragma'] = 'no-cache'
    $httpProvider.defaults.headers.common['Cache-Control'] = 'no-cache'
    $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest'
  ])
  .directive('eatClick', -> ((scope, element, attrs)-> $(element).click((ev)->ev.preventDefault())))
  .directive('ngModelOnblur', ->
    {
      restrict: 'A'
      require: 'ngModel'
      link: (scope, elm, attr, ngModelCtrl) ->
        return if attr.type == 'radio' || attr.type == 'checkbox'
        elm.unbind('input').unbind('keydown').unbind('change')
        elm.bind('blur', ->
          scope.$apply ->
            ngModelCtrl.$setViewValue(elm.val());
        )
    }
  )
  .directive('sendEvent', -> 
    restrict: 'A'
    scope:
      eventName: '@sendEvent'
      eventAttrs: '@eventAttrs'
    link: (scope, elm, attr) ->
      elm.click ($event) ->
        scope.$root.notify(scope.eventName, scope.$eval(scope.eventAttrs))
  )
  .directive('loadingContainer', ->
    restrict: 'A',
    scope: false,
    link: (scope, element, attrs) ->
      loadingLayer = angular.element('<div class="loading"></div>')
      element.append(loadingLayer)
      element.addClass('loading-container')
      scope.$watch(attrs.loadingContainer, (value) -> 
        loadingLayer.toggleClass('ng-hide', !value)
      )
  )
  .directive('daterangepicker', [() ->
    {
      restrict: 'EA'
      transclude: true
      scope:
        from: '='
        to: '='
        onRangeChange: '&'
        outDateFormat: '@'
        displayFormat: '@'
      template: """
              <div class="input-prepend">
        <button class="btn" type="button"><i class="fa fa-calendar-o"></i> Date range</button>
        </div>
      """
      replace: true
      link: (scope, element, attrs) ->
        
        outDateFormat = if angular.isDefined(attrs.outDateFormat) then attrs.outDateFormat else 'YYYY-MM-DD'
        displayFormat = if angular.isDefined(attrs.displayFormat) then attrs.displayFormat else "DD MMM, YYYY"
        ranges = if angular.isDefined(attrs.ranges) then attrs.ranges else 'past'
        scope.$watch (() -> "#{scope.from}#{scope.to}"), (() ->
          if scope.from && scope.to && $(element.find("input").val() == '')
            $(element).find("input").val(
              "#{moment(scope.from, outDateFormat).format(displayFormat)} - #{moment(scope.to, outDateFormat).format(displayFormat)}"
            )
          predefinedRanges = null
          if ranges == 'future'
            predefinedRanges = {
              'Today': [new Date(), new Date()],
              'Tomorrow': [moment().add('days', 1), moment().add('days', 1)],
              'Next 7 Days': [moment().add('days', 1), moment().add('days', 7)],
              'Next 30 Days': [moment().add('days', 1), moment().add('days', 30)],
              'Next month': [moment().add('month', 1).startOf('month'), moment().add('month', 1).endOf('month')]
            }
          else 
            predefinedRanges = {
             'Today': [new Date(), new Date()],
             'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
             'Last 7 Days': [moment().subtract('days', 6), new Date()],
             'Last 30 Days': [moment().subtract('days', 29), new Date()],
             'This Month': [moment().startOf('month'), moment().endOf('month')],
             'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
            }
          $(element).find("button").daterangepicker(
            {
              startDate: (moment(scope.from, outDateFormat) if scope.from),
              endDate: (moment(scope.to, outDateFormat) if scope.to),
              format: displayFormat,
              ranges: predefinedRanges,
              buttonClasses: ['btn-danger']
            },
            (from, to) ->
              [scope.from, scope.to] = [from.format(outDateFormat), to.format(outDateFormat)]
              scope.$apply()

              # Run the OnRangeChange handler _after_ changes were applied
              scope.onRangeChange() if scope.onRangeChange
          )

        ), true

    }
  ])

 
  