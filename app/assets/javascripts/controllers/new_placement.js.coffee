class window.NewPlacementController
	constructor: ($scope, $state, Placement,PlacementCategories) ->
		$scope.placementsizes = ['300x350','728x90','468x60','120x600','160x600']
		$scope.createPlacement = () ->

          	Placement.create({}, 
                {
                    name: $scope.placementName,
                    size: $scope.placementSize,
                    url: $scope.placementUrl,
                    category_ids: $scope.placementCategory
                },
                (p) ->
                    $state.transitionTo('dashboard',{placement_id: p.id})

            )
        $scope.categories = PlacementCategories.categories

window.NewPlacementController.$inject = ['$scope', '$state', 'Placement', 'PlacementCategories']