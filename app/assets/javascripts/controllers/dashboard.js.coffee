class window.DashboardController
  constructor: ($scope, $state, $timeout, Dashboard,ngTableParams,Placement) ->
    $scope.placementsTable = new ngTableParams(
      {
        page: 1 # show first page
        count: 5 # count per page
        total: 0
        sorting:
          name: "asc" # initial sorting
      },
      {
        getData: ($defer, params) -> 
        # ajax request to api
          Dashboard.query params.url(),(data) ->
            $timeout (-> 
              placements = data.placements
              $defer.resolve(placements.slice((params.page() - 1) * params.count(), params.page() * params.count()))
              params.total(placements.length)
            ),500
      }
    )

    $scope.inActivePlacement = (placement) ->
      result = confirm "Are you sure you want to make this placement InActive"

      if result 
        Placement.update({placement_id: placement.id}, active: false)
        placement.active = false

    $scope.setEditId =  (pid) ->
      $scope.editId = pid

    $scope.closeEditor = ->
      $scope.editId = -1

    $scope.getTextToCopy = ->
      console.log $scope.copied


window.DashboardController.$inject = ['$scope', '$state','$timeout', 'Dashboard','ngTableParams','Placement']