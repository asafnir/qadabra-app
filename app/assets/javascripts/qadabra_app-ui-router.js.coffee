window.$$qadabraAppModule.config(['$stateProvider', '$urlRouterProvider', ($stateProvider, $urlRouterProvider) ->
  $urlRouterProvider.otherwise("/")
  # $urlRouterProvider.when("/", ['$state', '$rootScope', ($state, $rootScope) ->
  #   return unless $rootScope.userLoggedIn == true || $rootScope.userLoggedIn == false
  #   dest = if $rootScope.userLoggedIn then 'dashboard' else 'user_sign_in'
  #   $state.transitionTo(dest)
  # ])
  
  $stateProvider
    .state('root', {
      url: '/',
      controller: window.RootController
    })
    .state('dashboard', {
      url: '/dashboard',
      views: {
        '':{
            templateUrl: '/html/dashboard/index.html',
            controller: window.DashboardController
          },
        'chart':{
            templateUrl: '/html/charts/index.html',
            controller: window.ChartController
        }
      }
    })
    .state('new_placement', {
      url: '/placements/new',
      templateUrl: '/html/placements/new.html',
      controller: window.NewPlacementController,
      resolve:{
        'PlacementCategories': (Placement) -> 
          Placement.get (data) ->
            return data.categories
      }
    })
])