//= require jquery
//= require jquery_ujs
//= require bootstrap
//= require unstable/angular
//= require unstable/angular-resource
//= require lib/angular-ui-router
//= require angular-ui-bootstrap


//= require lib/chosen.jquery.min
//= require lib/chosen
//= require lib/ng-google-chart
//= require lib/ng-table

//= require controllers/dashboard
//= require controllers/root
//= require controllers/new_placement
//= require controllers/chart

//= require qadabraApp
//= require qadabra_app-ui-router

//= require lib/moment
//= require lib/daterangepicker
