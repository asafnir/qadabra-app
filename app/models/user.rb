class User < ActiveRecord::Base
  
  devise :token_authenticatable, :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  attr_accessible :email, :password, :password_confirmation, :remember_me

  has_many :placements, foreign_key: :owner_id, :dependent => :destroy


  def clear_authentication_token!
    update_attribute(:authentication_token, nil)
  end
  
end
