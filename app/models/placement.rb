class Placement < ActiveRecord::Base
  
  attr_accessible :name, :size, :url, :category_ids, :owner_id, :active

  SIZES = ['300x350','728x90','468x60','120x600','160x600']

  belongs_to :owner, class_name: User
  validates_presence_of :name, :size, :owner
  
  validates :size, :inclusion => {:in => Placement::SIZES}

  SIZES.each do |size| #Add for each Placement the optin to do Placement.size
    scope size.gsub(' ','_').downcase.to_sym, lambda { where(size: size) }
  end

  has_many   :category_connections, as: :categoriable, dependent: :destroy
  has_many   :categories, through: :category_connections

end