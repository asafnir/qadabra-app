class Category < ActiveRecord::Base
  attr_accessible :name

  has_many :category_connections
  has_many :placements, through: :category_connections, source: :categoriable, source_type: 'placement'

end