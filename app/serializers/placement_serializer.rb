class PlacementSerializer < ActiveModel::Serializer
  attributes :id, :name, :size, :owner_id, :active, :category_ids
end
