class Api::PlacementsController < Api::BaseController

  before_filter :check_owner, only: [:show, :update, :destroy]

  def index
    @placements = current_user.placements
    #Use pluck as a shortcut to select one or more attributes without loading a bunch of records just to grab the attributes you want.
    @categories = Category.pluck(:name) 
    respond_to do |format|
      format.json  {
        render json: {
          placements: @placements, 
          categories: @categories
        }
      }
    end
  end

  def create
    placement = current_user.placements.create!(safe_params)
    render json: placement
  end

  def show
    render json: placement
  end

  def update
    placement.update_attributes(safe_params)
    render nothing: true, status: 204
  end

  def destroy
    placement.destroy
    render nothing: true
  end

  private
  def check_owner
    permission_denied if current_user != placement.owner
  end

  def placement
    @placement ||= Placement.find(params[:id])    
  end

  def safe_params
    params.require(:placement).permit(:name, :active, :size, :url, :category_ids)
  end

end
