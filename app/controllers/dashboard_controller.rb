class DashboardController < ApplicationController
  	before_filter :authenticate_user!

	def index
		respond_to do |format|
    		format.html { render inline: '', layout: 'application' }
    	end
  	end

end